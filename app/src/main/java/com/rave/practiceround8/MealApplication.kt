package com.rave.practiceround8

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Main class for dagger hilt.
 *
 * @constructor Create empty Meal application
 */
@HiltAndroidApp
class MealApplication : Application()
