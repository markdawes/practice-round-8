package com.rave.practiceround8.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.rave.practiceround8.databinding.FragmentCategoryBinding
import com.rave.practiceround8.viewmodel.MealViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * A simple [Fragment] subclass.
 */
@AndroidEntryPoint
class CategoryFragment : Fragment() {

    private var _binding: FragmentCategoryBinding? = null
    private val binding get() = _binding!!

    private val viewModel by viewModels<MealViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return FragmentCategoryBinding.inflate(inflater, container, false).apply {
            _binding = this
            val adapter = CategoryAdapter()
            viewModel.categories.observe(
                viewLifecycleOwner,
                Observer {
                    adapter.setData(it)
                }
            )
            binding.rvCategories.adapter = adapter
            binding.rvCategories.layoutManager = LinearLayoutManager(requireContext())
        }.root
    }
}
