package com.rave.practiceround8.model.local

/**
 * Class for category data.
 *
 * @property idCategory
 * @property strCategory
 * @property strCategoryDescription
 * @property strCategoryThumb
 * @constructor Create empty Category
 */
data class Category(
    val idCategory: String,
    val strCategory: String,
    val strCategoryDescription: String,
    val strCategoryThumb: String
)
