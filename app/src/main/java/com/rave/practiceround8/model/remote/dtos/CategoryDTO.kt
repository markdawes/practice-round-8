package com.rave.practiceround8.model.remote.dtos

import kotlinx.serialization.Serializable

@Serializable
data class CategoryDTO(
    val idCategory: String,
    val strCategory: String,
    val strCategoryDescription: String,
    val strCategoryThumb: String
)
