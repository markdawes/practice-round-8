package com.rave.practiceround8.model.remote

import com.rave.practiceround8.model.remote.dtos.CategoryResponse
import retrofit2.http.GET

/**
 * Api service.
 *
 * @constructor Create empty A p i service
 */
interface APIService {

    @GET(CATEGORY_ENDPOINT)
    suspend fun getMealCategories(): CategoryResponse

    companion object {
        private const val CATEGORY_ENDPOINT = "categories.php"
    }
}
